GUILE=guile2.2 # the name might be different on your system

TESTS=tests/net/client.scm

test: $(TESTS)
	$(GUILE) -L "." $(TESTS)
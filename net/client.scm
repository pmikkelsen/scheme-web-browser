(define-module (net client)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 rdelim)
  #:export (make-client
            close
            send
            recieve-all
            recieve-line))

(define (make-client address port) ; port can be a string with a port number, or a service like "http"
  (let* ((ai (car (getaddrinfo address port)))
         (family (addrinfo:fam ai))
         (addr (addrinfo:addr ai))
         (sock (socket family SOCK_STREAM 0)))
    (connect sock addr)
    sock))

(define (close client)
  (shutdown client 2))

(define (send client data)
  (begin
    (display data client)
    (force-output client)))

(define (recieve-all client)
  (get-string-all client))

(define (recieve-line client)
  (read-line client))
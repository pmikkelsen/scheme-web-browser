(define-module (tests net client)
  #:use-module ((net client) #:prefix net:)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen))

(define message "Hello network!")

(define server (open-input-output-pipe "nc -l 127.0.0.1 12345"))
(sleep 1)

(define client (net:make-client "localhost" "12345"))

(display (string-append message "\n") server)
(force-output server)

(define output (string-append (net:recieve-line client)
                              "\n"))

(net:send client output)

(if (string=? message (read-line server))
  (display "OK")
  (display "FAIL"))
(newline)